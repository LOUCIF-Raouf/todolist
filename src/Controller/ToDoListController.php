<?php

namespace App\Controller;

use App\Entity\Item;
use App\Entity\User;
use App\Form\ItemType;
use App\Repository\ToDoListRepository;
use App\Service\ItemService;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ToDoListController extends AbstractController
{
    /**
     * @Route("/{id}/todolist", name="to_do_list")
     */
    public function index(ToDoListRepository $todoListRepository, User $user, Request $request, ItemService $itemService, MailerService $mailerService)
    {
        $item = new Item();
        $form = $this->createForm(ItemType::class, $item);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $item->setCreatedAt(new \DateTime());
            $item->setToDoList($user->getToDoList());

            if ($itemService->canAddItem($item) == $item && $mailerService->sendMail($user)) {
                $entityManager->persist($item);
                $entityManager->flush();
            } else {
                if (!$mailerService->sendMail($user))
                    $this->addFlash('warning', 'Opps ! Vous avez moins de 18 ans');
                else
                    $this->addFlash('warning', 'Opps ! L\'item n\'est pas valide');

            }

            return $this->redirectToRoute('to_do_list', [
                'id' => $user->getId()
            ]);


        }

        return $this->render('to_do_list/index.html.twig', [
            'controller_name' => 'ToDoListController',
            'todo' => $todoListRepository->findOneBy(['user' => $user]),
            'form' => $form->createView(),

        ]);
    }


}
