<?php


namespace App\Service;


use App\Entity\Item;
use App\Entity\ToDoList;
use App\Repository\ItemRepository;

class ItemService
{

    private $itemRepository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    
    public function canAddItem(Item $item)
    {

        if (
            !empty($item->getName())
            && !empty($item->getContent())
            && !empty($item->getCreatedAt())
            && !empty($item->getToDoList())
            && strlen($item->getContent()) < 1000
            && $this->countItems($item->getToDoList()) < 10
            && $this->minutesDiff($item) >= 30
        ) {
            return $item;

        }

        return null;
    }

    public function countItems(ToDoList $todolist)
    {
        if ($todolist)
            return count($todolist->getItems());

        return -1;
    }

    public function minutesDiff(Item $item)
    {

        return $item->getCreatedAt()->diff(new \DateTime($this->itemRepository->findByLastDate()[0][1]),true)->i;

    }

}
