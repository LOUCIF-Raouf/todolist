<?php


namespace App\Service;


use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;

class MailerService
{

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendMail(User $user)
    {
        if ($user->getAge() >= 18) {
            $message = (new TemplatedEmail())
                ->from('kkidszen@gmail.com')
                ->to('kkidszen@gmail.com')
                ->subject('Tests Units Add Item ')
                ->text('ça va !')
                ->htmlTemplate('emails/email.html.twig');
            $sentTokenEmail = $this->mailer->send($message);
            return true;
        } else {

            return false;
        }


    }

}
