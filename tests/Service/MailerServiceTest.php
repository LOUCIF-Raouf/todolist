<?php


namespace App\Tests\Service;


use App\Entity\Item;
use App\Entity\ToDoList;
use App\Entity\User;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Mailer\MailerInterface;

class MailerServiceTest extends KernelTestCase
{

    private $item;
    private $user;
    private $todo;

    protected function setUp()
    {

        $user = new User();
        $user->setFirstname('Alex')
            ->setLastname('Angelino')
            ->setEmail("testUnit@gmail.com")
            ->setPassword('password8password')
            ->setAge((new \DateTime())->diff(new \DateTime('-91 years'), true)->y);

        $todo = new ToDoList();
        $todo->setUser($user)
            ->setName('To do list 1')
            ->setCreatedAt(new \DateTime());
        $user->setToDoList($todo);

        $item = new Item();
        $item->setToDoList($todo)
            ->setName('Item 1')
            ->setContent('Ceci est un test unit de Item class')
            ->setCreatedAt(new \DateTime());

        $todo->addItem($item);

        $this->item = $item;
        $this->todo = $todo;
        $this->user = $user;


    }

    public function testMailerAgeMoreThan18Valid()
    {

        $mailerInterface = $this->createMock(MailerInterface::class);
        $mailerInterface->expects($this->any())
            ->method('send');

        $mailer = new MailerService($mailerInterface);

        $this->user->getAge(23);

        $this->assertTrue($mailer->sendMail($this->user));
    }

    public function testMailerAgeLessThan18Valid()
    {

        $mailerInterface = $this->createMock(MailerInterface::class);
        $mailerInterface->expects($this->any())
            ->method('send');

        $mailer = new MailerService($mailerInterface);

        $this->user->setAge(16);

        $this->assertFalse($mailer->sendMail($this->user));
    }


}
