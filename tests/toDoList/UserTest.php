<?php


namespace App\Tests\toDoList;


use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{

    private $user;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $user= new User();
        $user->setFirstname('Raouf')
            ->setLastname('Serra')
            ->setEmail("testperf@gmail.com")
            ->setPassword('mlnkbbbbbbbbbbbbbbbbbiebeebeioele')
            ->setAge((new \DateTime())->diff(new \DateTime('-19 years'),true)->y);

        $this->user =$user;

    }



    public function testUserValid(){
            $this->assertTrue($this->user->IsValid());
    }

    public function testLastNameNoValid(){
        $this->user->setLastname('');
            $this->assertFalse($this->user->IsValid());
    }

    public function testFirstNameNoValid(){
        $this->user->setFirstname('');
        $this->assertFalse($this->user->IsValid());
    }

    public function testPasswordNullNoValid(){
        $this->user->setPassword('');
        $this->assertFalse($this->user->IsValid());
    }

    public function testPasswordLenghtShortNoValid(){
        $this->user->setPassword('pass');
        $this->assertFalse($this->user->IsValid());
    }

    public function testPasswordLenghtLongNoValid(){
        $this->user->setPassword('passpasspasspasspasspasspasspasspasspasspasspasspass');
        $this->assertFalse($this->user->IsValid());
    }

    public function testEmailNoValid(){
        $this->user->setEmail('email');
        $this->assertFalse($this->user->IsValid());
    }

    public function testAgeNoValid(){
        $this->user->setAge(12);
        $this->assertFalse($this->user->IsValid());
    }

}
